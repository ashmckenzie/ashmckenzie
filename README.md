## About Me

- Engineer at GitLab since April 2018.
- I value being courteous, honest, genuine, grateful, kind and helpful.

## [GitLab values](https://about.gitlab.com/handbook/values/) that especially resonate with me

| Core Value | Value |
| ---------- | ------ |
| [🤝 Collaboration](https://about.gitlab.com/handbook/values/#collaboration) | [Say thanks](https://about.gitlab.com/handbook/values/#say-thanks) · [ Say sorry](https://about.gitlab.com/handbook/values/#say-sorry) · [ No ego](https://about.gitlab.com/handbook/values/#no-ego) |
| [📈 Results](https://about.gitlab.com/handbook/values/#results) | [Dogfooding](https://about.gitlab.com/handbook/values/#dogfooding) · [ Give agency](https://about.gitlab.com/handbook/values/#give-agency) · [ Bias for action](https://about.gitlab.com/handbook/values/#bias-for-action) |
| [⏱️ Efficiency](https://about.gitlab.com/handbook/values/#efficiency) | [Boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions) · [ Be respectful of others' time](https://about.gitlab.com/handbook/values/#be-respectful-of-others-time) · [ Managers of one](https://about.gitlab.com/handbook/values/#managers-of-one)
| [🌐 Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) | [Bias towards asynchronous communication](https://about.gitlab.com/handbook/values/#bias-towards-asynchronous-communication) · [ See Something, Say Something](https://about.gitlab.com/handbook/values/#see-something-say-something) · [ Family and friends first, work second](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second) |
| [👣 Iteration](https://about.gitlab.com/handbook/values/#iteration) | [Don't wait](https://about.gitlab.com/handbook/values/#dont-wait) · [ Embracing Iteration](https://about.gitlab.com/handbook/values/#embracing-iteration) · [ Make small merge requests](https://about.gitlab.com/handbook/values/#make-small-merge-requests) |
| [👁️ Transparency](https://about.gitlab.com/handbook/values/#transparency) | [Public by default](https://about.gitlab.com/handbook/values/#public-by-default) · [ Single Source of Truth](https://about.gitlab.com/handbook/values/#single-source-of-truth) · [ Say why, not just what](https://about.gitlab.com/handbook/values/#say-why-not-just-what) |

## Links

- [My GitLab profile](https://gitlab.com/ashmckenzie)
- [My GitHub profile](https://github.com/ashmckenzie)

You can learn more about me in my [personal README](https://gitlab.com/ashmckenzie/readme).
